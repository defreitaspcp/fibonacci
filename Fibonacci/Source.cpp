/*
Author: de Freitas, P.C.P
Description: Fibonacci Sequence
*/
#include<iostream>
using namespace std;
int fibonacci(int n)
{
	if (n<=0)
	{
		//cout << "Term greater than zero" << endl;
		return -1;
	}
	else
	{
		if (n >= 1 && n <= 3)
		{
			return n - 1;
		}
		else
		{
			return fibonacci(n-1) + fibonacci(n-2);
		}
	}
}
int main()
{
	cout << fibonacci(0) << endl;
	cout << fibonacci(1) << endl;
	cout << fibonacci(2) << endl;
	cout << fibonacci(3) << endl;
	cout << fibonacci(4) << endl;
	cout << fibonacci(5) << endl;
	while (true)
	{

	}
	return 0;
}